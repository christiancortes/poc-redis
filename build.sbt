name := "POC-redis"

version := "0.1"

scalaVersion := "2.12.10"


libraryDependencies += "dev.profunktor" %% "redis4cats-effects" % "0.9.2"

libraryDependencies +="io.circe"                   %% "circe-core"              % "0.13.0"
libraryDependencies +="io.circe"                   %% "circe-parser"            % "0.13.0"
libraryDependencies +="io.circe"                   %% "circe-generic"           % "0.13.0"


libraryDependencies += "dev.profunktor" %% "redis4cats-streams" % "0.9.2"

libraryDependencies += "dev.profunktor" %% "redis4cats-log4cats" % "0.9.2"

libraryDependencies += "io.chrisdavenport"  %% "log4cats-slf4j" % "1.0.1"
