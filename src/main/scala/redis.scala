import cats.effect.{IO, Resource}
import cats.syntax.apply._
import dev.profunktor.redis4cats.algebra.RedisCommands
import dev.profunktor.redis4cats.codecs.Codecs
import dev.profunktor.redis4cats.codecs.splits.SplitEpi
import dev.profunktor.redis4cats.connection.{RedisClient, RedisURI}
import dev.profunktor.redis4cats.domain.RedisCodec
import dev.profunktor.redis4cats.interpreter.Redis
import dev.profunktor.redis4cats.log4cats._
import io.chrisdavenport.log4cats.Logger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import io.circe.{Decoder, Encoder}
import io.circe.syntax._



object HelloWorld {
  def main(args: Array[String]): Unit = {

    implicit val cs = IO.contextShift(scala.concurrent.ExecutionContext.global)
    implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    case class User2(id:String, name:String)


    implicit val decodeUser: Decoder[User2] = {
      (
        Decoder.instance(_.downField("id").as[String]),
        Decoder.instance(_.downField("name").as[String]),
        ).mapN(User2(_, _))
    }

    implicit val encodeUser: Encoder[User2] =
      Encoder.forProduct2("name", "idUser")(user =>
        (user.id, user.name))

    val userCodec: SplitEpi[String, User2] =
      SplitEpi(
        s => {
        println("llllllllllllllllllllllllllllllllllll")
        println(s.asJson.noSpaces)
        s.asJson.as[User2].toTry
        .fold(_ => User2("invalid", "invalid"), user => user)
      }, _.toString)


    val longCodec: RedisCodec[String, User2] = Codecs.derive[String, User2](RedisCodec.Utf8, userCodec)

    val commandsApi: Resource[IO, RedisCommands[IO, String, User2]] =
      for {
        uri    <- Resource.liftF(RedisURI.make[IO]("redis://localhost"))
        client <- RedisClient[IO](uri)
        redis  <- Redis[IO, String, User2](client, longCodec)
      } yield redis

/*
    def putStrLn(str: String): IO[Unit] = IO(println(str))
    import cats.effect.IO
    import dev.profunktor.redis4cats.effects._
    import io.lettuce.core.GeoArgs

    val testKey = "location"

    def putStrLn(str: String): IO[Unit] = IO(println(str))

    val _BuenosAires  = GeoLocation(Longitude(-58.3816), Latitude(-34.6037), "Buenos Aires")
    val _RioDeJaneiro = GeoLocation(Longitude(-43.1729), Latitude(-22.9068), "Rio de Janeiro")
    val _Montevideo   = GeoLocation(Longitude(-56.164532), Latitude(-34.901112), "Montevideo")
    val _Tokyo        = GeoLocation(Longitude(139.6917), Latitude(35.6895), "Tokyo")

    commandsApi.use { cmd => // GeoCommands[IO, String, String]
      for {
        _ <- cmd.geoAdd(testKey, _BuenosAires)
        _ <- cmd.geoAdd(testKey, _RioDeJaneiro)
        _ <- cmd.geoAdd(testKey, _Montevideo)
        _ <- cmd.geoAdd(testKey, _Tokyo)
        x <- cmd.geoDist(testKey, _BuenosAires.value, _Tokyo.value, GeoArgs.Unit.km)
        _ <- putStrLn(s"Distance from ${_BuenosAires.value} to Tokyo: $x km")
        y <- cmd.geoPos(testKey, _RioDeJaneiro.value)
        _ <- putStrLn(s"Geo Pos of ${_RioDeJaneiro.value}: ${y.headOption}")
        z <- cmd.geoRadius(testKey, GeoRadius(_Montevideo.lon, _Montevideo.lat, Distance(10000.0)), GeoArgs.Unit.km)
        _ <- putStrLn(s"Geo Radius in 1000 km: $z")
      } yield ()
    }
*/
    commandsApi.use(connection =>
      connection.set("christian",User2("Christian","Cortes"))
    ).unsafeRunSync()

    commandsApi.use(connection2 => {
      connection2.get("christian")
          .map(_.map(user => {
            println("*******************+")
            println(user.name)

            println("**************************")
            println(user.id)

          })).unsafeRunSync()

      connection2.get("christian")

    }

    ).map(l => l.fold(print("noExist"))
    (l => print(l))).unsafeRunSync()

  }
}

